#-------------------------------------------------
#
# Project created by QtCreator 2015-08-12T17:14:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = testQtDataBase
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h
